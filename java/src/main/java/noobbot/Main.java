package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashSet;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import data.objects.BotId;
import data.objects.Data;
import data.objects.DataGameInit;
import data.objects.DataJoin;
import data.objects.DataLapFinished;
import data.objects.DataYourCar;
import data.objects.Datum;
import data.objects.Id;
import data.objects.TipoPista;

public class Main {
	DataGameInit gameInit;
	int tick = 0;
	static int test = 0;
	int indexInicioCurva;
	double distCurva;
	TrackPieceList pieceList;
	double velBoa = 100;
	boolean turbo = false;
	int myID;
	boolean podeMelhorar = true;
	double angQuePassou = 0;
	boolean flagSwitch = true;
	int laneAnterior = -1;
	boolean rightLeft = true;
	double anguloQuePassou;
	boolean init = true;
	boolean retaFinal = false;
	double balanceThrottle = 0.5;
	double distCent = 0;
	double aceleration = 0;
	public static void main(String... args) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
 
		new Main(reader, writer, new Join(botName, botKey));
		System.out.println(test);
		//new Main(reader, writer, new JoinRace(botName, botKey, null));
	}

	final Gson gson = new Gson();
	private PrintWriter writer;
	public static HashSet<String> indexComDesempenhoJaAumentadoNaVolta = new HashSet<String>();
	private static Datum meuCarro = new Datum();

	public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg join) throws IOException {
		this.writer = writer;
		String line = null;

		send(join);

		Double anguloAnterior = 0.0;
		Double posicaoAnterior = null;

		while ((line = reader.readLine()) != null) {
			// System.out.println(line);
			final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);

			if (msgFromServer.msgType.equals("yourCar")) {

				System.out.println("YOUR CAR - Testando...12");
				DataYourCar dataYourCar = gson.fromJson(msgFromServer.data.toString(), DataYourCar.class);
				meuCarro.setId(new Id());
				meuCarro.getId().setColor(dataYourCar.getColor());
				meuCarro.getId().setName(dataYourCar.getName());
				System.out.println(dataYourCar);

			} else if (msgFromServer.msgType.equals("carPositions")) {
				System.out.println("#######################NOVO CAR POSITION2#####################");
				tick++;
				List<Datum> listaInfo = gson.fromJson(msgFromServer.data.toString(), new TypeToken<List<Datum>>() {
				}.getType());
				int posicaoCarro = listaInfo.indexOf(meuCarro);
				Datum datum = listaInfo.get(posicaoCarro);

				// Obtendo hist�rico de ponto na pista
				// PontoNaPista pontoNaPista =
				// Main2.mapa.get(datum.getPiecePosition().getPieceIndex() + "_"
				// + datum.getPiecePosition().getInPieceDistance().intValue());

				// System.out.println(pontoNaPista);   
				Double velocidadeAtual = Main2.velocidadeAtual(posicaoAnterior, datum.getPiecePosition().getInPieceDistance());

				System.out.println("##VELOCIDADE ATUAL: " + velocidadeAtual + " m/click");
				System.out.println("##ANGULO ATUAL: " + datum.getAngle());
				System.out.println("##NOVA DIST PARA CURVA: " + distCurva);
				System.out.println("##TICK: " + tick);
				System.out.println("##velBoa: " + velBoa);
				System.out.println("angulo que passou: " + angQuePassou);
				System.out.println(gameInit.getRace().getTrack().getLanes().size());
				System.out.println(datum.getPiecePosition().getLane().getEndLaneIndex());
				System.out.println("Distance from Center: " + distCent);
				System.out.println("lane anterior" + laneAnterior);
				System.out.println("balanceThrottle: " + balanceThrottle);
				// Melhorando a performance
				if (Math.abs(datum.getAngle()) > 5) {
					if (pieceList.getCurentPiece().getType().equals(TipoPista.CURVA)) {
						if (!podeMelhorar && (Math.abs(angQuePassou) < Math.abs(anguloAnterior))) {
							angQuePassou = Math.abs(anguloAnterior);
						}
						if (podeMelhorar && Math.abs(pieceList.getCurentPiece().getVelBoa(distCent) - velocidadeAtual) < 0.4 && Math.abs(angQuePassou) < Math.abs(anguloAnterior)) {
							System.out.println("#############D� para melhorar#############");
							podeMelhorar = false;
							angQuePassou = Math.abs(anguloAnterior);
						}
					}
				}

				if (!podeMelhorar && pieceList.getCurentPiece().getType().equals(TipoPista.RETA)) {
					System.out.println("angulo que passou: " + angQuePassou);
					pieceList.getCurentPiece().getPrevious().setMelhoria(Main2.qtdDeMelhora(angQuePassou));
					podeMelhorar = true;
				}

				pieceList.checkPieceChanged(datum.getPiecePosition().getPieceIndex());
				distCurva = pieceList.getDistCurva(gameInit.getRace().getTrack().getPieces(), datum.getPiecePosition().getPieceIndex(), datum.getPiecePosition().getInPieceDistance());

				if (gameInit.getRace().getRaceSession().getLaps() != null && datum.getPiecePosition().getLap().equals(gameInit.getRace().getRaceSession().getLaps() - 1) && pieceList.getCurentPiece().getNext().isLinhaDeChegada()) {
					retaFinal = true;
				}

				for (Datum carro : listaInfo) {
					if (!carro.getId().equals(meuCarro.getId()) && TrackPieceList.contains(pieceList.getCurentPiece().getIndexComposition(), carro.getPiecePosition().getPieceIndex())) {
						if (carro.getPiecePosition().getLane().getEndLaneIndex().equals(datum.getPiecePosition().getLane().getEndLaneIndex()) && Math.abs(pieceList.carDist(gameInit.getRace().getTrack().getPieces(), datum.getPiecePosition(), carro.getPiecePosition())) < 200) {
							if (flagSwitch && pieceList.getCurentPiece().getNext().getAngle() > 0 && datum.getPiecePosition().getLane().getEndLaneIndex() + 1 < gameInit.getRace().getTrack().getLanes().size()) {
								send(new SwitchLane("Right"));
								System.out.println("Turn Right");
								flagSwitch = false;
							} else if (pieceList.getCurentPiece().getNext().getAngle() < 0 && datum.getPiecePosition().getLane().getEndLaneIndex() > 0) {
								send(new SwitchLane("Left"));
								System.out.println("Turn Left");
								flagSwitch = false;
							}
						}
					}
				}
				if(datum.getPiecePosition().getLane().getEndLaneIndex() != laneAnterior){
					distCent = gameInit.getRace().getTrack().getLanes().get(datum.getPiecePosition().getLane().getEndLaneIndex()).getDistanceFromCenter();
				}
				
				aceleration = 0.12 + Math.pow(velocidadeAtual/50,2);
				
				
				
				if (!turbo && pieceList.getCurentPiece().getType().equals(TipoPista.RETA) && flagSwitch && pieceList.getCurentPiece().getNumberOfSwitches() > 0) {
					if (pieceList.getCurentPiece().getNext().getAngle() > 0 && datum.getPiecePosition().getLane().getEndLaneIndex() + 1 < gameInit.getRace().getTrack().getLanes().size()) {
						send(new SwitchLane("Right"));
						distCent = gameInit.getRace().getTrack().getLanes().get(datum.getPiecePosition().getLane().getEndLaneIndex() +1).getDistanceFromCenter();
						System.out.println("Turn Right");
						flagSwitch = false;
					} else if (pieceList.getCurentPiece().getNext().getAngle() < 0 && datum.getPiecePosition().getLane().getEndLaneIndex() > 0) {
						send(new SwitchLane("Left"));
						gameInit.getRace().getTrack().getLanes().get(datum.getPiecePosition().getLane().getEndLaneIndex()-1).getDistanceFromCenter();
						System.out.println("Turn Left");
						flagSwitch = false;
					}
				}

				if (turbo && pieceList.getCurentPiece().getType().equals(TipoPista.RETA) && pieceList.getCurentPiece().getLength() >=500 && (pieceList.getDistCurva(gameInit.getRace().getTrack().getPieces(), datum.getPiecePosition().getPieceIndex(), datum.getPiecePosition().getInPieceDistance()) > 500 || (retaFinal && pieceList.getCurentPiece().isLinhaDeChegada()))) {
					System.err.println("                                                     TURBOOO!");
					send(new Turbo());
					turbo = false;
				} else if (Math.abs(datum.getAngle()) >= 50) {
					if (Math.abs(datum.getAngle()) < Math.abs(anguloAnterior)) {
						System.out.println("Acelerando - Angulo alto!!");
						send(new Throttle(1.0));
					} else {
						System.out.println("Brecando - Angulo alto!!");
						send(new Throttle(0.0));
					}
				} else if (velocidadeAtual < 0) {
					System.out.println("ping");
					send(new Ping());
				} else if (pieceList.getCurentPiece().getType().equals(TipoPista.RETA)) {
					if (retaFinal && pieceList.getCurentPiece().isLinhaDeChegada()) {
						System.out.println("Acelerando na Reta FINAL!");
						send(new Throttle(1.0));
					} else if (Main2.precisaBrecar(distCurva, velocidadeAtual, pieceList.getCurentPiece().getNext().getVelBoa(distCent), aceleration)) {
						System.out.println("Desacelerando na Reta");
						send(new Throttle(0.0));
					} else {
						System.out.println("Acelerando na Reta");
						send(new Throttle(1.0));
					}
				} else if (pieceList.getCurentPiece().getType().equals(TipoPista.CURVA)){
					if (pieceList.getCurentPiece().getNext().getType().equals(TipoPista.CURVA) && Main2.precisaBrecar(distCurva, velocidadeAtual, pieceList.getCurentPiece().getNext().getVelBoa(distCent), aceleration)) {
						System.out.println("Desacelerando na Curva 'Curva menor ainda na frente'");
						send(new Throttle(0.1));
					} 
					else if (pieceList.getCurentPiece().getNext().getType().equals(TipoPista.RETA) && Main2.precisaBrecar(distCurva, velocidadeAtual, pieceList.getCurentPiece().getNext().getNext().getVelBoa(distCent), aceleration)){
						System.out.println("Desacelerando na Curva 'reta pequena com curva grande a frente'");
						send(new Throttle(0.1));
					}
					else if (Math.abs(datum.getAngle()) < Math.abs(anguloAnterior) && datum.getAngle() / pieceList.getCurentPiece().getAngle() > 0 && !Main2.precisaBrecar(distCurva, velocidadeAtual, pieceList.getCurentPiece().getNext().getVelBoa(distCent), aceleration)) {
						System.out.println("Acelerando na Curva - Angulo diminuindo");
						send(new Throttle(1.0));
					} else if (velocidadeAtual > pieceList.getCurentPiece().getVelBoa(distCent)) {
						System.out.println("Brecando na Curva - velocidade Alta demais");
						balanceThrottle = Main2.getThrottle(false, balanceThrottle);
						send(new Throttle(balanceThrottle));
					} else if (velocidadeAtual < pieceList.getCurentPiece().getVelBoa(distCent)){
						System.out.println("Acelerando na Curva - velocidade Baixa demais");
						balanceThrottle = Main2.getThrottle(true, balanceThrottle);
						send(new Throttle(balanceThrottle));
					} else {
						System.out.println("Acelerando na Curva - Else");
						send(new Throttle(1.0));
					}
				} else{
					System.out.println("Acelerando");
					send(new Throttle(1.0));
				}
					

				if (datum.getPiecePosition().getLane().getEndLaneIndex() != laneAnterior) {
					flagSwitch = true;
				}
				laneAnterior = datum.getPiecePosition().getLane().getEndLaneIndex();
				if (pieceList.getCurentPiece().getType().equals(TipoPista.RETA)) {
					System.out.println("velBoa + melhoria: " + pieceList.getCurentPiece().getNext().getVelocidadeNecessaria() + "    " + pieceList.getCurentPiece().getNext().getMelhoria());
					velBoa = pieceList.getCurentPiece().getNext().getVelBoa(distCent);
				} else {
					velBoa = pieceList.getCurentPiece().getVelBoa(distCent);
					System.out.println("velBoa + melhoria: " + pieceList.getCurentPiece().getVelocidadeNecessaria() + "    " + pieceList.getCurentPiece().getMelhoria());
				}
				posicaoAnterior = datum.getPiecePosition().getInPieceDistance();
				anguloAnterior = datum.getAngle();

			} else if (msgFromServer.msgType.equals("turboAvailable")) {
				turbo = true;

			} else if (msgFromServer.msgType.equals("gameInit")) {

				System.out.println("Race init");
				System.out.println(msgFromServer.data.toString());
				gameInit = gson.fromJson(msgFromServer.data.toString(), DataGameInit.class);
				if (init) {
					pieceList = new TrackPieceList(gameInit);
					init = false;
				}

				System.out.println(msgFromServer.data.toString());
				//Main2.carregarMapa(gameInit);

				// System.out.println(Main2.mapa);

				System.out.println(gameInit);

			} else if (msgFromServer.msgType.equals("gameEnd")) {
				podeMelhorar = true;
				flagSwitch = true;
				balanceThrottle = 0.5;
				System.out.println("Race end");
				System.out.println(msgFromServer.data.toString());
				// DataGameEnd gameEnd =
				// gson.fromJson(msgFromServer.data.toString(),
				// DataGameEnd.class);
				// System.out.println(gameEnd);

			} else if (msgFromServer.msgType.equals("join")) {

				System.out.println("Joined");
				DataJoin joinData = gson.fromJson(msgFromServer.data.toString(), DataJoin.class);
				System.out.println(joinData);

			} else if (msgFromServer.msgType.equals("lapFinished")) {

				System.out.println("lapFinished");
				DataLapFinished lapFinished = gson.fromJson(msgFromServer.data.toString(), DataLapFinished.class);
				System.out.println("limpando indexComDesempenhoJaAumentadoNaVolta");
				indexComDesempenhoJaAumentadoNaVolta.clear();
				System.out.println(lapFinished);

			} else if (msgFromServer.msgType.equals("gameStart")) {

				System.out.println("Race start");
				System.out.println(msgFromServer.data);
				

			} else if (msgFromServer.msgType.equals("crash")) {
				System.out.println("crash");
				pieceList.getCurentPiece().melhoria *= 0.9;
				flagSwitch = true;
			} else {

				System.out.println("####OUTROSSSSS " + msgFromServer.msgType + " " + msgFromServer.data);

				send(new Ping());

			}
			// //////////////////////////////////////////////////////////////////////////////////////

			// if (msgFromServer.msgType.equals("carPositions")) {
			// send(new Throttle(0.5));
			// } else if (msgFromServer.msgType.equals("join")) {
			// System.out.println("Joined");
			// } else if (msgFromServer.msgType.equals("gameInit")) {
			// System.out.println("Race init");
			// } else if (msgFromServer.msgType.equals("gameEnd")) {
			// System.out.println("Race end");
			// } else if (msgFromServer.msgType.equals("gameStart")) {
			// System.out.println("Race start");
			// } else {
			// send(new Ping());
			// }
		}
	}

	private void send(final SendMsg msg) {
		writer.println(msg.toJson());
		writer.flush();
	}
}

abstract class SendMsg {
	public String toJson() {
		return new Gson().toJson(new MsgWrapper(this));
	}

	protected Object msgData() {
		return this;
	}

	protected abstract String msgType();
}

class MsgWrapper {
	public final String msgType;
	public final Object data;

	MsgWrapper(final String msgType, final Object data) {
		this.msgType = msgType;
		this.data = data;
	}

	public MsgWrapper(final SendMsg sendMsg) {
		this(sendMsg.msgType(), sendMsg.msgData());
	}

}

class Join extends SendMsg {
	public final String name;
	public final String key;

	Join(final String name, final String key) {
		this.name = name;
		this.key = key;
	}

	@Override
	protected String msgType() {
		return "join";
	}
}

class JoinRace extends SendMsg {
	public final Data data;

	JoinRace(final String name, final String key, String trackName) {
		this.data = new Data();
		this.data.setBotId(new BotId());
		this.data.getBotId().setKey(key);
		this.data.getBotId().setName(name);
		this.data.setCarCount(3);
		this.data.setTrackName(trackName);
	}

	@Override
	protected Object msgData() {
		return data;
	}

	@Override
	protected String msgType() {
		return "joinRace";
	}
}

class Ping extends SendMsg {
	@Override
	protected String msgType() {
		return "ping";
	}
}

class Throttle extends SendMsg {
	private double value;

	public Throttle(double value) {
		this.value = value;
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "throttle";
	}
}

class SwitchLane extends SendMsg {
	private String side;

	public SwitchLane(String side) {
		this.side = side;
	}

	@Override
	protected Object msgData() {
		return side;
	}

	@Override
	protected String msgType() {
		return "switchLane";
	}
}

class Turbo extends SendMsg {
	private String data;

	public Turbo() {
		this.data = "turbo";
	}

	@Override
	protected Object msgData() {
		return data;
	}

	@Override
	protected String msgType() {
		return "turbo";
	}
}
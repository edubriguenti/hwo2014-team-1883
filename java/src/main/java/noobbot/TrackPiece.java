package noobbot;

import data.objects.TipoPista; 

public class TrackPiece {
	private TipoPista type;
	private TrackPiece next;
	private TrackPiece previous;
	private boolean linhaDeChegada = false;
	private double posLinhaDeChegada = 0;
	private double length = 0;
	
	
	private double angle = 0;
	private double radius = 0;
	public int[] indexComposition;
	public double velocidadeNecessaria;
	public double melhoria = 1.0;
	public int numberOfSwitches = 0;
	
	public TrackPiece() {
		this.length = 0;
		this.angle = 0;
		this.radius = 0;
	}

	public TipoPista getType() {
		return type;
	}

	public void setType(TipoPista type) {
		this.type = type;
	}

	public int getNumberOfSwitches() {
		return numberOfSwitches;
	}

	public void setNumberOfSwitches(int numberOfSwitches) {
		this.numberOfSwitches = numberOfSwitches;
	}

	public TrackPiece getNext() {
		return next;
	}

	public void setNext(TrackPiece next) {
		this.next = next;
	}

	public TrackPiece getPrevious() {
		return previous;
	}

	public void setPrevious(TrackPiece previous) {
		this.previous = previous;
	}

	public boolean isLinhaDeChegada() {
		return linhaDeChegada;
	}

	public void setLinhaDeChegada(boolean linhaDeChegada) {
		this.linhaDeChegada = linhaDeChegada;
	}

	public double getPosLinhaDeChegada() {
		
		
		
		return posLinhaDeChegada;
	}

	public void setPosLinhaDeChegada(double posLinhaDeChegada) {
		this.posLinhaDeChegada = posLinhaDeChegada;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getAngle() {
		return angle;
	}

	public double getVelocidadeNecessaria() {
		return velocidadeNecessaria;
	}

	public void setVelocidadeNecessaria(double velocidadeNecessaria) {
		this.velocidadeNecessaria = velocidadeNecessaria;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public int[] getIndexComposition() {
		return indexComposition;
	}

	public void setIndexComposition(int[] indexComposition) {
		this.indexComposition = indexComposition;
	}

	public double getMelhoria() {
		return melhoria;
	}

	public void setMelhoria(double melhoria) {
		this.melhoria += melhoria;
	} 
	
	public double getVelBoa(double radiusDifference){
		if(this.getAngle() > 0){
			return ( Math.pow((this.getRadius()-radiusDifference)/2.16, 0.5))*this.melhoria;
		}
		else return ( Math.pow((this.getRadius()+radiusDifference)/2.16, 0.5))*this.melhoria;
	}
}


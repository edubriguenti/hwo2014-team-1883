package noobbot;

import java.text.DecimalFormat;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import data.objects.DataGameInit;
import data.objects.MomentoCriacaoPonto;
import data.objects.Piece;
import data.objects.PontoNaPista;
import data.objects.TipoPista;

public class Main2 {

	static Deque<Integer> pilhaVelocidade = new ArrayDeque<Integer>();
	static int testeMain2 = 0;
	static List<Integer> ponto_aceleracao = new ArrayList<Integer>();
	static List<Integer> ponto_frenagem = new ArrayList<Integer>();
	public static DecimalFormat decimalFormat = new DecimalFormat("#");

	static Double velocidadeIdealAngulo45 = 0.0;

	// mapa - A chave � a posi��o / e o valor � a velocidade que o carro deve
	// estar naquele ponto
	static Map<String, PontoNaPista> mapa = new LinkedHashMap<String, PontoNaPista>();

	static Map<Integer, List<PontoNaPista>> mapaCurvas = new LinkedHashMap<Integer, List<PontoNaPista>>();

	public static void carregarMapa(DataGameInit dataGameInit) {

		List<Piece> pieces = dataGameInit.getRace().getTrack().getPieces();
		int pecaIndex = 0;
		boolean ehNovaCurva = true;

		ArrayList<PontoNaPista> curva = null;
		int indexCurva = 0;

		for (Piece piece : pieces) {
			if (piece.getLength() != null) {
				ehNovaCurva = true;
				for (int i = 0; i < piece.getLength(); i++) {
					// System.out.println("MAPEANDO RETA " + "" + pecaIndex +
					// "_" + i);
					mapa.put("" + pecaIndex + "_" + i, new PontoNaPista("" + pecaIndex + "_" + i, TipoPista.RETA, 0.0, distanciaParaCurva(pieces, pecaIndex, i), proximaCurvaEhForte(pieces, pecaIndex, i), MomentoCriacaoPonto.INICAL, false, piece.getRadius(), piece.getAngle()));

				}

			} else {
				// CURVAAAA
				Double comprimentoCurva = calcularComprimentoCurva(piece);
				Double distanciaParaRetaInicial = distanciaParaReta(pieces, pecaIndex, comprimentoCurva, 0.0);

				if (ehNovaCurva) {
					if (curva != null) {
						mapaCurvas.put(indexCurva, curva);
					}
					curva = new ArrayList<PontoNaPista>();
					indexCurva = pecaIndex;
				} else {
					curva = (ArrayList<PontoNaPista>) mapaCurvas.get(indexCurva);
				}

				for (double i = 0.0; i < comprimentoCurva; i++) {
					// System.out.println("MAPEANDO CURVA " + pecaIndex + "_" +
					// decimalFormat.format(i));
					Double distanciaParaRetaDoPonto = distanciaParaReta(pieces, pecaIndex, comprimentoCurva, i);
					mapa.put("" + pecaIndex + "_" + decimalFormat.format(i),
							new PontoNaPista("" + pecaIndex + "_" + decimalFormat.format(i), TipoPista.CURVA, 0.0, distanciaParaRetaDoPonto, calcularNivelCurva(piece), MomentoCriacaoPonto.INICAL, i > distanciaParaRetaInicial / 2 ? true : false, piece.getRadius(), piece.getAngle()));

					curva.add(mapa.get("" + pecaIndex + "_" + decimalFormat.format(i)));

				}
			}
			pecaIndex++;
		}

		// System.out.println("");
		// Test.serializar(mapaCurvas);
		// mapa = Test.deserializar();
	}

	public static boolean precisaBrecar(Double distancia, Double velocidadeAtual, Double velocidadeNecessariaNaCurva, Double desaceleracao) {

		return (distancia / velocidadeAtual) < (velocidadeAtual - velocidadeNecessariaNaCurva) / desaceleracao;

	}

	private static List<String> posicoesAntes(int numeroDePosicoesAntes, Map<String, PontoNaPista> mapa, String posicao) {
		List<String> arrayPosicoesAntes = new ArrayList<String>();
		boolean encontrou = false;
		List<String> keySet = new ArrayList<String>(mapa.keySet());

		for (String posicaoAtual : keySet) {
			if (posicaoAtual.equals(posicao)) {
				encontrou = true;
				break;
			} else {

				// S� vou adicionar Reta
				// if(mapa.get(posicaoAtual).getTipo().equals(TipoPista.RETA)){
				arrayPosicoesAntes.add(posicaoAtual);
				// }
			}
		}

		if (!encontrou) {
			// System.err.println("######ERRROOOOOO ... NAO FOI POSSIVEL MELHORAR");
			return new ArrayList<String>();
		}

		if (arrayPosicoesAntes.size() == numeroDePosicoesAntes) {
			return arrayPosicoesAntes;
		} else if (arrayPosicoesAntes.size() > numeroDePosicoesAntes) {
			int posicoesAEleminar = arrayPosicoesAntes.size() - numeroDePosicoesAntes;
			arrayPosicoesAntes = arrayPosicoesAntes.subList(posicoesAEleminar, arrayPosicoesAntes.size() - 1);
		} else {

			// Se o tamanho da lista for menor

			int posicoesAAdicionar = numeroDePosicoesAntes - arrayPosicoesAntes.size();
			arrayPosicoesAntes.addAll(keySet.subList(keySet.size() - posicoesAAdicionar, keySet.size()));
		}

		return arrayPosicoesAntes;
	}

	private static Integer proximaCurvaEhForte(List<Piece> pieces, int indiceAtual, int lengthAtual) {

		for (int i = indiceAtual + 1; i < pieces.size(); i++) {
			Piece piece = pieces.get(i);

			if (piece.getLength() == null) {

				if (Math.abs(piece.getAngle()) >= 45.0 && piece.getRadius() < 100) {
					return 2;
				} else if (Math.abs(piece.getAngle()) >= 45.0) {
					return 1;
				} else {
					return 0;
				}

			} else {

			}

		}

		return 0;
	}

	private static Integer calcularNivelCurva(Piece piece) {

		if (piece.getLength() == null) {

			if (Math.abs(piece.getAngle()) >= 45.0 && piece.getRadius() < 100) {
				return 2;
			} else if (Math.abs(piece.getAngle()) >= 45.0) {
				return 1;
			} else {
				return 0;
			}
		}

		return 0;
	}

	private static Double calcularComprimentoCurva(Piece piece) {
		Double comprimentoCurva = (Math.abs(piece.getAngle()) * Math.PI * piece.getRadius()) / 180;
		return comprimentoCurva;
	}

	public static Double distanciaParaCurva(List<Piece> pieces, int indiceAtual, int lengthAtual) {

		Double distanciaParaCurva = 0.0;

		Piece pecaAtual = pieces.get(indiceAtual);
		distanciaParaCurva = pecaAtual.getLength() - lengthAtual;

		for (int i = indiceAtual + 1; i < pieces.size(); i++) {
			Piece piece = pieces.get(i);

			if (piece.getLength() != null) {
				distanciaParaCurva = distanciaParaCurva + piece.getLength();
			} else {
				distanciaParaCurva = distanciaParaCurva + distanciaParaReta(pieces, indiceAtual, calcularComprimentoCurva(piece), 0.0) / 2;
				break;
			}

		}

		return distanciaParaCurva;

	}

	public static Double distanciaParaReta(List<Piece> pieces, int indiceAtual, Double lengthTotal, Double lenghtAtual) {

		Double distanciaParaReta = 0.0;

		distanciaParaReta = lengthTotal - lenghtAtual;

		for (int i = indiceAtual + 1; i < pieces.size(); i++) {
			Piece piece = pieces.get(i);

			if (piece.getLength() == null) {
				distanciaParaReta = distanciaParaReta + calcularComprimentoCurva(piece);
			} else {
				break;
			}

		}

		return distanciaParaReta;

	}

	public static Double velocidadeAtual(Double posAnterior, Double posAtual) {
		if (posAnterior != null && posAtual != null) {

			Double velocidade = (posAtual - posAnterior);
			return velocidade;

		}

		return 0.0;
	}

	public static void aumentarPerfomance(String chave, int pontoDaPeca, double ang) {
		List<String> posicoesAntes = posicoesAntes(100, mapa, chave);
		for (String string : posicoesAntes) {
			PontoNaPista pontoNaPista = mapa.get(string);

			if (!Main.indexComDesempenhoJaAumentadoNaVolta.contains(string)) {
				pontoNaPista.setVelocidadeNecessariaNoPonto(pontoNaPista.getVelocidadeNecessariaNoPonto() * qtdDeMelhora(ang));
			}

			Main.indexComDesempenhoJaAumentadoNaVolta.add(string);

		}

	}

	public static double qtdDeMelhora(double ang) {

		double a;
		if ((50 - ang) > 0)
			a = -0.035 + Math.pow((ang / 3) + 1, -1.17);
		// a = ((50 - ang) / 100) * 0.15;
		else
			a = -0.03;
		System.out.println("melhoria: " + a);
		return a;
	}

	public static double getThrottle(boolean maisOuMenos, double throttleAnterior) { // mais = true, menos = false
		if (maisOuMenos == true) {
			return (throttleAnterior + (1 - throttleAnterior) / 2);
		} else {
			return (throttleAnterior - (throttleAnterior) / 2);
		}
	}
}
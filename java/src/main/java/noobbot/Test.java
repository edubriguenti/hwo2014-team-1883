//package noobbot;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.nio.charset.Charset;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//import java.util.AbstractMap;
//import java.util.HashMap;
//import java.util.LinkedHashMap;
//import java.util.Map;
//
//import com.thoughtworks.xstream.XStream;
//import com.thoughtworks.xstream.converters.Converter;
//import com.thoughtworks.xstream.converters.MarshallingContext;
//import com.thoughtworks.xstream.converters.UnmarshallingContext;
//import com.thoughtworks.xstream.io.HierarchicalStreamReader;
//import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
//
//import data.objects.PontoNaPista;
//
//public class Test {
//
//    public static void serializar(Map args) {
//
//        XStream magicApi = new XStream();
//        //magicApi.registerConverter(new MapEntryConverter());
//        //magicApi.useAttributeFor(PontoNaPista.class);
//       // magicApi.alias("root", Map.class);
//
//        String xml = magicApi.toXML(args);
//        System.out.println("Result of tweaked XStream toXml()");
//        System.out.println(xml);
//
//        write(xml);
//        
//        //Map<String, String> extractedMap = (Map<String, String>) magicApi.fromXML(xml);
//
//    }
//    
//    public static Map deserializar(){
//    	XStream magicApi = new XStream();
//    	
//    	String xml = "";
//        try {
//	        xml = readFile("C:/Users/Eduardo/Desktop/teste.txt", Charset.defaultCharset());
//        } catch (IOException e) {
//	        // TODO Auto-generated catch block
//	        e.printStackTrace();
//        }
//    	
//    	 Map<String, PontoNaPista> extractedMap = (LinkedHashMap<String, PontoNaPista>) magicApi.fromXML(xml);
//    	 
//    	 return extractedMap;
//    }
//    
//    static String readFile(String path, Charset encoding) 
//    		  throws IOException 
//    		{
//    		  byte[] encoded = Files.readAllBytes(Paths.get(path));
//    		  return new String(encoded, encoding);
//    		}
//
//    public static class MapEntryConverter implements Converter {
//
//        public boolean canConvert(Class clazz) {
//            return AbstractMap.class.isAssignableFrom(clazz);
//        }
//
//        public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context) {
//
//            AbstractMap map = (AbstractMap) value;
//            for (Object obj : map.entrySet()) {
//                Map.Entry entry = (Map.Entry) obj;
//                writer.startNode(entry.getKey().toString());
//                writer.setValue(entry.getValue().toString());
//                writer.endNode();
//            }
//
//        }
//
//        public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
//
//            Map<String, String> map = new HashMap<String, String>();
//
//            while(reader.hasMoreChildren()) {
//                reader.moveDown();
//
//                String key = reader.getNodeName(); // nodeName aka element's name
//                String value = reader.getValue();
//                map.put(key, value);
//
//                reader.moveUp();
//            }
//
//            return map;
//        }
//
//    }
//    
//    public static void write(String content) {
//    	 
//		File file = new File("C:/Users/Eduardo/Desktop/teste.txt");
// 
//		try (FileOutputStream fop = new FileOutputStream(file)) {
// 
//			// if file doesn't exists, then create it
//			if (!file.exists()) {
//				file.createNewFile();
//			}
// 
//			// get the content in bytes
//			byte[] contentInBytes = content.getBytes();
// 
//			fop.write(contentInBytes);
//			fop.flush();
//			fop.close();
// 
//			System.out.println("Done");
// 
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
//
//}
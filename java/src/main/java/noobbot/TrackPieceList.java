package noobbot;

import java.util.ArrayList;
import java.util.List;

import data.objects.DataGameInit;
import data.objects.Piece;
import data.objects.PiecePosition;
import data.objects.TipoPista;

public class TrackPieceList {
	private TrackPiece curentPiece;

	public TrackPieceList(DataGameInit gameInit) {
		int i = 0;
		int startIndex = 0;
		int endIndex = 0;
		List<Piece> pieces = gameInit.getRace().getTrack().getPieces();
		List<TrackPiece> trackPieces = new ArrayList<TrackPiece>();
		while (i < pieces.size()) {
			TrackPiece curentPiece = new TrackPiece();
			if (pieces.get(i).getLength() != null) { // If(Reta)
				curentPiece.setType(TipoPista.RETA);
				startIndex = i;
				while (i < pieces.size() && pieces.get(i) != null && pieces.get(i).getLength() != null) { // While(piece != null && piece == Reta)
					curentPiece.setLength(curentPiece.getLength() + pieces.get(i).getLength());
					i++;
				}
				endIndex = i;
			} else {
				curentPiece.setType(TipoPista.CURVA);
				double curentRadius = pieces.get(i).getRadius();
				boolean PositNegat = pieces.get(i).getAngle() >= 0; // verifica se o angulo da curva � positivo ou negativo.... true = positivo, false = negativo
				curentPiece.setRadius(curentRadius);
				startIndex = i;
				while (i < pieces.size() && pieces.get(i) != null && pieces.get(i).getAngle() != null && pieces.get(i).getRadius() == curentRadius && PositNegat == pieces.get(i).getAngle() >= 0) { // While(piece != null &&  piece == Curva && raio da curva n�o muda && lado da curva nao muda)
					curentPiece.setAngle(curentPiece.getAngle() + pieces.get(i).getAngle());
					i++;
				}
				endIndex = i;
				curentPiece.setLength(Math.abs(curentPiece.getAngle()) * Math.PI * curentPiece.getRadius() / 180);
				//curentPiece.setVelocidadeNecessaria( 6.4 * Math.pow(curentPiece.getRadius()/100, 0.55) );
				curentPiece.setVelocidadeNecessaria(Math.pow(curentPiece.getRadius()/2.16, 0.5));
				
			}
			curentPiece.indexComposition = new int[i - startIndex];
			for (int j = 0; j < endIndex - startIndex; j++) {
				curentPiece.indexComposition[j] = j + startIndex;
			}
			trackPieces.add(curentPiece);
		}
		if (trackPieces.get(0).getType() == trackPieces.get(trackPieces.size() - 1).getType()) {
			int[] newComp = merge(trackPieces.get(trackPieces.size() - 1).indexComposition, trackPieces.get(0).indexComposition);
			trackPieces.get(0).indexComposition = newComp;
			trackPieces.get(0).setLinhaDeChegada(true);
			trackPieces.get(0).setPosLinhaDeChegada((trackPieces.get(0).getLength() + trackPieces.get(trackPieces.size() - 1).getLength()) - trackPieces.get(0).getLength());
			trackPieces.get(0).setLength(trackPieces.get(0).getLength() + trackPieces.get(trackPieces.size() - 1).getLength());
			trackPieces.remove(trackPieces.size() - 1);
		} else {
			trackPieces.get(0).setLinhaDeChegada(true);
			trackPieces.get(0).setPosLinhaDeChegada(0);
		}
		for (i = 0; i < trackPieces.size() - 1; i++) {
			trackPieces.get(i + 1).setPrevious(trackPieces.get(i));
			trackPieces.get(i).setNext(trackPieces.get(i + 1));
			for( int j = 0; j < trackPieces.get(i).getIndexComposition().length; j++){
				if(gameInit.getRace().getTrack().getPieces().get(trackPieces.get(i).getIndexComposition()[j]).getSwitch() != null){
					trackPieces.get(i).numberOfSwitches++;
				}
			}
		};
		this.curentPiece = trackPieces.get(0);
		trackPieces.get(0).setPrevious(trackPieces.get(trackPieces.size() - 1));
		trackPieces.get(0).setNext(trackPieces.get(1));
		trackPieces.get(trackPieces.size() - 1).setNext(trackPieces.get(0));
		// trackPieces.get(trackPieces.size() - 1).setNext(trackPieces.get(0));
	}

	public TrackPiece getCurentPiece() {
		return curentPiece;
	}

	public void setCurentPiece(TrackPiece newPiece) {
		this.curentPiece = newPiece;
	}

	public static int[] merge(int[] a, int[] b) {
		int[] c = new int[a.length + b.length];
		int i;
		for (i = 0; i < a.length; i++)
			c[i] = a[i];
		for (int j = 0; j < b.length; j++)
			c[i++] = b[j];
		return c;
	}

	public void checkPieceChanged(int pieceIndex) {
		while(!contains(curentPiece.getIndexComposition(), pieceIndex)){
			setCurentPiece(curentPiece.getNext());
		}
	}
	
	public double getDistCurva(List<Piece> pieces, int curentPieceIndex, double curentInPieceDistance) {
		double distanciaProxCurva = 9999;
		int j = 0;
		int i = curentPiece.getIndexComposition()[j];
		if (curentPiece.getType().equals(TipoPista.RETA)) {
			distanciaProxCurva = curentPiece.getLength();
			while (i != curentPieceIndex && curentPiece.getType().equals(TipoPista.RETA)) {
				distanciaProxCurva -= pieces.get(i).getLength();
				i = curentPiece.getIndexComposition()[++j];
			}
		}
		else if(curentPiece.getNext().getType().equals(TipoPista.CURVA)){
			distanciaProxCurva = curentPiece.getLength();
			double radius = curentPiece.getRadius();
			while (i != curentPieceIndex && curentPiece.getRadius() == radius) {
				distanciaProxCurva -= Math.abs(pieces.get(i).getAngle()) * Math.PI * pieces.get(i).getRadius()/180;
				i = curentPiece.getIndexComposition()[++j];
			}
		}
		else{
			distanciaProxCurva = curentPiece.getLength() + curentPiece.getNext().getLength();
			double radius = curentPiece.getRadius();
			while (i != curentPieceIndex && curentPiece.getRadius() == radius) {
				distanciaProxCurva -= Math.abs(pieces.get(i).getAngle()) * Math.PI * pieces.get(i).getRadius()/180;
				i = curentPiece.getIndexComposition()[++j];
			}
		}
		distanciaProxCurva -= curentInPieceDistance;
		return distanciaProxCurva;
	}
	
	
	public static boolean contains(int[] array, int number){
		for(int i = 0; i < array.length; i++)
			if(number == array[i])
				return true;
		return false;
	}
	
	public double carDist(List<Piece> pieces, PiecePosition carro1, PiecePosition carro2){
		return getDistCurva(pieces, carro1.getPieceIndex(), carro1.getInPieceDistance()) - getDistCurva(pieces, carro2.getPieceIndex(), carro2.getInPieceDistance());
	}
	
}

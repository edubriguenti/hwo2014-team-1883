package data.objects;


public class PontoNaPista {

	private String id;
	private TipoPista tipo;
	private Double velocidadeNecessariaNoPonto;
	private Double distanciaParaACurvaOuReta;
	
	//CURVAAA
	private boolean curvaPassouDaMetade;
	private Integer raio;
	private Double angulo;
	private Integer nivelCurva;

	public PontoNaPista(String id, TipoPista tipo, Double velocidadeNecessariaNoPonto, Double distanciaParaACurvaOuReta, Integer nivelCurva, MomentoCriacaoPonto momento, boolean curvaPassouDaMetade, Integer raio, Double angulo) {
		super();
		this.id = id;
		this.tipo = tipo;
		this.velocidadeNecessariaNoPonto = velocidadeNecessariaNoPonto;
		this.distanciaParaACurvaOuReta = distanciaParaACurvaOuReta;
		this.curvaPassouDaMetade = curvaPassouDaMetade;
		this.raio = raio;
		this.angulo = angulo;
		this.nivelCurva = nivelCurva;
		
		if (momento.equals(MomentoCriacaoPonto.INICAL)) {

			if (tipo.equals(TipoPista.RETA)) {
				if (nivelCurva == 1){
					
					if (distanciaParaACurvaOuReta > 100) {
	
						this.velocidadeNecessariaNoPonto = 10.0;
	
					} else if (distanciaParaACurvaOuReta > 75) {
	
						this.velocidadeNecessariaNoPonto = 9.0;
	
					}else if (distanciaParaACurvaOuReta > 50) {
	
						this.velocidadeNecessariaNoPonto = 8.0;
	
					} 
					else if (distanciaParaACurvaOuReta > 0) {
						this.velocidadeNecessariaNoPonto = 7.0;
					}
					
				}else if (nivelCurva == 2){
					if (distanciaParaACurvaOuReta > 120) {
						
						this.velocidadeNecessariaNoPonto = 7.0;
	
					} else if (distanciaParaACurvaOuReta > 75) {
	
						this.velocidadeNecessariaNoPonto = 5.5;
	
					}else if (distanciaParaACurvaOuReta > 50) {

						this.velocidadeNecessariaNoPonto = 5.0;
	
					} 
					else if (distanciaParaACurvaOuReta > 0) {
						this.velocidadeNecessariaNoPonto = 4.0;
					}
					
				} else {
					
					this.velocidadeNecessariaNoPonto = 10.0;
					
				}


			}

			if (tipo.equals(TipoPista.CURVA)) {
				if (nivelCurva == 0){
					this.velocidadeNecessariaNoPonto = 10.0;
				}else if (nivelCurva == 1){
					this.velocidadeNecessariaNoPonto = 6.0;
				}else if (nivelCurva == 2){
					this.velocidadeNecessariaNoPonto = 4.0;
				}
			}
		}

	}
	
	public Double calcularVelocidadeInicialNoPonto(Double s0, Double s, Double vf) {
	    Double v0 = null;
	    
	    v0 = Math.pow(vf, 2) - (2.0 * (0.11 * (s - s0)));
	    v0 = Math.sqrt(v0);
	    
		
		return v0;
    }

	public TipoPista getTipo() {
		return tipo;
	}

	public void setTipo(TipoPista tipo) {
		this.tipo = tipo;
	}

	public Double getVelocidadeNecessariaNoPonto() {
		return velocidadeNecessariaNoPonto;
	}

	public void setVelocidadeNecessariaNoPonto(Double velocidadeNecessariaNoPonto) {
		this.velocidadeNecessariaNoPonto = velocidadeNecessariaNoPonto;
	}

	public Double getDistanciaParaACurvaOuReta() {
		return distanciaParaACurvaOuReta;
	}

	public void setDistanciaParaACurvaOuReta(Double distanciaParaACurvaOuReta) {
		this.distanciaParaACurvaOuReta = distanciaParaACurvaOuReta;
	}
	
	public boolean isCurvaPassouDaMetade() {
		return curvaPassouDaMetade;
	}

	public void setCurvaPassouDaMetade(boolean curvaPassouDaMetade) {
		this.curvaPassouDaMetade = curvaPassouDaMetade;
	}
	
	public Integer getRaio() {
		return raio;
	}

	public void setRaio(Integer raio) {
		this.raio = raio;
	}
	
	

	public Double getAngulo() {
		return angulo;
	}

	public void setAngulo(Double angulo) {
		this.angulo = angulo;
	}

	public Integer getNivelCurva() {
		return nivelCurva;
	}

	public void setNivelCurva(Integer nivelCurva) {
		this.nivelCurva = nivelCurva;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		if (tipo.equals(TipoPista.RETA)) {
			return "id "+ id + " " +tipo + " ###Dist�ncia para a pr�xima Curva: " + distanciaParaACurvaOuReta + " ###Velocidade desejada:" + velocidadeNecessariaNoPonto+ " ###Raio:" + raio+ " ###curvaPassouDaMetade:" + curvaPassouDaMetade+ " ###angulo:" + angulo+ " ###nivelCurva:" + nivelCurva;
		} else {
			return "id "+ id + " " +tipo + " ###Dist�ncia para a pr�xima Reta: " + distanciaParaACurvaOuReta + " ###Velocidade desejada:" + velocidadeNecessariaNoPonto+ " ###Raio:" + raio+ " ###curvaPassouDaMetade:" + curvaPassouDaMetade+ " ###angulo:" + angulo+ " ###nivelCurva:" + nivelCurva;
		}

	}

}

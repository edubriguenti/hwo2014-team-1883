
package data.objects;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class RaceSession {

    @Expose
    private Integer laps;
    @Expose
    private Integer maxLapTimeMs;
    @Expose
    private Boolean quickRace;

    public Integer getLaps() {
        return laps;
    }

    public void setLaps(Integer laps) {
        this.laps = laps;
    }

    public Integer getMaxLapTimeMs() {
        return maxLapTimeMs;
    }

    public void setMaxLapTimeMs(Integer maxLapTimeMs) {
        this.maxLapTimeMs = maxLapTimeMs;
    }

    public Boolean getQuickRace() {
        return quickRace;
    }

    public void setQuickRace(Boolean quickRace) {
        this.quickRace = quickRace;
    }

}

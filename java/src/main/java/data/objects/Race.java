
package data.objects;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class Race {

    @Expose
    private Track track;
    @Expose
    private List<Car> cars = new ArrayList<Car>();
    @Expose
    private RaceSession raceSession;

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    public RaceSession getRaceSession() {
        return raceSession;
    }

    public void setRaceSession(RaceSession raceSession) {
        this.raceSession = raceSession;
    }

}

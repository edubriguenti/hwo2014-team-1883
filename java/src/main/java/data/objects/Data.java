
package data.objects;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class Data {

    @Expose
    private BotId botId;
    @Expose
    private String trackName;
    @Expose
    private Integer carCount;

    public BotId getBotId() {
        return botId;
    }

    public void setBotId(BotId botId) {
        this.botId = botId;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public Integer getCarCount() {
        return carCount;
    }

    public void setCarCount(Integer carCount) {
        this.carCount = carCount;
    }

}


package data.objects;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class DataLapFinished {

    @Expose
    private Car car;
    @Expose
    private LapTime lapTime;
    @Expose
    private RaceTime raceTime;
    @Expose
    private Ranking ranking;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public LapTime getLapTime() {
        return lapTime;
    }

    public void setLapTime(LapTime lapTime) {
        this.lapTime = lapTime;
    }

    public RaceTime getRaceTime() {
        return raceTime;
    }

    public void setRaceTime(RaceTime raceTime) {
        this.raceTime = raceTime;
    }

    public Ranking getRanking() {
        return ranking;
    }

    public void setRanking(Ranking ranking) {
        this.ranking = ranking;
    }

}

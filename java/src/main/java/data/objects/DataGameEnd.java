
package data.objects;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class DataGameEnd {

    @Expose
    private List<Result> results = new ArrayList<Result>();
    @Expose
    private List<BestLap> bestLaps = new ArrayList<BestLap>();

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public List<BestLap> getBestLaps() {
        return bestLaps;
    }

    public void setBestLaps(List<BestLap> bestLaps) {
        this.bestLaps = bestLaps;
    }

}

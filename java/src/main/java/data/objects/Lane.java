package data.objects;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class Lane {

	@Expose
	private Integer distanceFromCenter;
	@Expose
	private Integer index;
	@Expose
	private Integer startLaneIndex;
	@Expose
	private Integer endLaneIndex;

	public Integer getDistanceFromCenter() {
		return distanceFromCenter;
	}

	public void setDistanceFromCenter(Integer distanceFromCenter) {
		this.distanceFromCenter = distanceFromCenter;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public Integer getStartLaneIndex() {
		return startLaneIndex;
	}

	public void setStartLaneIndex(Integer startLaneIndex) {
		this.startLaneIndex = startLaneIndex;
	}

	public Integer getEndLaneIndex() {
		return endLaneIndex;
	}

	public void setEndLaneIndex(Integer endLaneIndex) {
		this.endLaneIndex = endLaneIndex;
	}

}
